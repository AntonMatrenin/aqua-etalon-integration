const AuthService = require('./auth');
const mssql = require('mssql');

const LINKED_SERVER = AuthService.LINKED_SERVER;

const getClientDebts = async (pool, clientId) => {
  try {
    const result = await pool.request().input('idklient', mssql.Int, clientId)
      .query(`
        SELECT
          t1.arendadolg,
          t1.salesdolgs,
          t1.zalogdolg,
          t1.zalogdolgs
        FROM
          [${LINKED_SERVER}]...[klients] t1
        WHERE
          t1.id=@idklient
      `);

    const { recordset } = result;

    return {
      rentDebt: recordset[0].arendadolg,
      waterDebt: recordset[0].salesdolgs,
      waterCount: recordset[0].salesdolg,
      pledgeCount: recordset[0].zalogdolg,
      pledgeDebt: recordset[0].zalogdolgs,
    };
  } catch (err) {
    // Ignore integration if something went wrong
    console.log(`[getClientDebts] ${JSON.stringify(err)}`);
    return '';
  }
};

module.exports = {
  getClientDebts,
};
