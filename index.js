const Koa = require('koa');
const Router = require('koa-router');
const fs = require('fs');
const util = require('util');
const AuthService = require('./services/auth');
const AddressService = require('./services/address');
const DebtService = require('./services/debt');
const DiscountService = require('./services/discount');

const app = new Koa();
const router = new Router();

const logFile = fs.createWriteStream('log.txt', { flags: 'a' });
const logStdout = process.stdout;

console.log = function () {
  logFile.write(util.format.apply(null, arguments) + '\n');
  logStdout.write(util.format.apply(null, arguments) + '\n');
};

console.error = console.log;

router.get('/', ctx => {
  ctx.body = 'OK';
});

router.get('/logs', async ctx => {
  ctx.body = fs.readFileSync('log.txt', 'utf8');
});

router.get('/address', async ctx => {
  ctx.body = await AddressService.getClientAddresses(
    ctx.state.pool,
    ctx.state.clientId
  );
});

router.get('/debt', async ctx => {
  ctx.body = await DebtService.getClientDebts(
    ctx.state.pool,
    ctx.state.clientId
  );
});

router.get('/discount', async ctx => {
  ctx.body = await DiscountService.getClientDiscount(
    ctx.state.pool,
    ctx.state.clientId
  );
});

app
  .use(async (ctx, next) => {

    if (ctx.header.token !== process.env.TOKEN && ctx.originalUrl !== '/logs') {
      throw new Error('invalid token');
    } else {
      return next();
    }
  })
  .use(async (ctx, next) => {
    if(ctx.originalUrl === '/logs') {
      return next();
    }

    ctx.state.pool = await AuthService.dbConnect();

    ctx.state.clientId = await AuthService.getClientIdByPhone(
      ctx.state.pool,
      ctx.header.phone
    );

    if (!ctx.state.clientId) {
      ctx.body = '';
      return;
    }

    return next();
  })
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(process.env.PORT);
