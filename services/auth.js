const mssql = require('mssql');
const isEmpty = require('is-empty');
const memoize = require('lodash/memoize');

const LINKED_SERVER = 'TEST6';

const dbConnect = async () => {
  const pool = new mssql.ConnectionPool(process.env.DB_URL);

  return pool.connect();
};

const _getClientIdByPhone = async (pool, phone) => {
  try {
    const result = await pool.request().input('phone', mssql.VarChar, phone)
      .query(`
        SELECT
          t1.id
        FROM
          [${LINKED_SERVER}]...[klients] t1
            INNER JOIN [${
              LINKED_SERVER
            }]...[klientphone] t2 ON t1.id=t2.idklient
            INNER JOIN [${LINKED_SERVER}]...[phones] t3 ON t2.idphone=t3.id
        WHERE
          SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(t3.longname,'-',''),' ',''),'(',''),')',''), 1, 10)=@phone
      `);

    const { recordset } = result;

    if (isEmpty(recordset)) {
      return '';
    }

    return recordset[0].id || '';
  } catch (err) {
    // Ignore integration if something went wrong
    console.log(`[getUserIdByPhone] ${JSON.stringify(err)}`);
    return '';
  }
};

const getClientIdByPhone = memoize(_getClientIdByPhone, (_, phone) => phone);

module.exports = {
  getClientIdByPhone,
  dbConnect,
  LINKED_SERVER,
};
