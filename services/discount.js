const AuthService = require('./auth');
const mssql = require('mssql');

const LINKED_SERVER = AuthService.LINKED_SERVER;

const getClientDiscount = async (pool, clientId) => {
  try {
    const result = await pool.request().input('idklient', mssql.Int, clientId)
      .query(`
        SELECT 
          MAX((t2.price - t1.price)) discount
        FROM 
          [${LINKED_SERVER}]...[docnormal] t1 INNER JOIN [${
      LINKED_SERVER
    }]...[products] t2 ON t1.idprod=t2.id
        WHERE 
          t1.idklient=@idklient AND t2.istara=1 
      `);

    const { recordset } = result;

    if (recordset.length === 0) {
      return 0;
    }

    return recordset[0].discount;
  } catch (err) {
    // Ignore integration if something went wrong
    console.log(`[getClientDiscount] ${JSON.stringify(err)}`);
    return '';
  }
};

module.exports = {
  getClientDiscount,
};
