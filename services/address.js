const AuthService = require('./auth');
const mssql = require('mssql');
const trim = require('lodash/trim');

const LINKED_SERVER = AuthService.LINKED_SERVER;

const getClientAddresses = async (pool, clientId) => {
  try {
    const result = await pool.request().input('idklient', mssql.Int, clientId)
      .query(`
        SELECT
          t1.longname
        FROM
          [${LINKED_SERVER}]...[adres] t1
            INNER JOIN [${LINKED_SERVER}]...[klientadres] t2 ON t1.id=t2.idadres
        WHERE
          t2.idklient=@idklient
      `);

    const { recordset } = result;

    return recordset.map(item => trim(item.longname));
  } catch (err) {
    // Ignore integration if something went wrong
    console.log(`[getClientAddresses] ${JSON.stringify(err)}`);
    return '';
  }
};

module.exports = {
  getClientAddresses,
};
